<?php

namespace Contract;

interface GiftableInterface {

    public function offer();

}
