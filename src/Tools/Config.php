<?php

namespace Tools;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class Config {

    public static array $instance;

    public static function get($value)
    {
        if(empty(self::$instance[$value])) {
            self::$instance[$value] = self::getFromFile($value);
        }

        return self::$instance[$value];
    }

    public static function getFromFile($value)
    {
        $parsed = explode('.', $value);

        $configFile = $parsed[0];

        $configFile = __DIR__ . '/../../config/' . $configFile . '.php';

        if(file_exists($configFile)) {

            $config = include $configFile;

            return isset($parsed[1]) ? $config[$parsed[1]] : $config;
        }
        else {
            throw new FileNotFoundException($configFile);
        }
    }

    public static function set($key, $value)
    {
        self::$instance[$key] = $value;
    }


}



