<?php

namespace Tools;

use \Firebase\JWT\JWT as FirebaseJwt;

class Jwt {

    public static function encode(array $data)
    {
        $payload = array(
            "iss"   =>  Config::get('jwt.iss'),
            "iat"   =>  Config::get('jwt.iat'),
            "exp"   =>  Config::get('jwt.exp'),
        );

        return FirebaseJwt::encode(array_merge($payload, $data), Config::get('jwt.key'), 'HS256');
    }


}

