<?php

namespace Tools;


use Entity\User;
use Repository\UserRepository;

class Auth {

    protected static User $user;

    public static function setUser($email)
    {
        $user = (new UserRepository())->findByEmail($email);

        static::$user = $user;
    }

    public static function user()
    {
        return self::$user;
    }
}

