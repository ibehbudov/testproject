<?php

namespace Repository;

use Entity\User;
use Tools\Config;
use Tools\Jwt;

class UserRepository {

    protected $entityManager;

    protected User $user;

    public function __construct()
    {
        $this->entityManager = Config::get('entityManager')->getRepository(User::class);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function checkCredentials($email, $password)
    {
        $account = $this->entityManager->findOneBy(['email' => $email]);

        if($account) {
            $this->setUser($account);
        }

        return $account && password_verify($password, $account->getPassword());
    }

    public function generateUserJwtToken()
    {
        return Jwt::encode([
            'email' => $this->getUser()->getEmail()
        ]);
    }

    public function findByEmail(string $email)
    {
        return $this->entityManager->findOneBy(['email' => $email]);
    }


}

