<?php

namespace Http\Middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use Http\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;
use Tools\Auth;
use Tools\Config;

class AuthMiddleware extends ApiController {

    public function handle(Request $request)
    {
        $bearerToken = $request->headers->get('Authorization');
        $bearerToken = trim(strstr($bearerToken, ' '));

        try {
            $decoded = JWT::decode($bearerToken, new Key(Config::get('jwt.key'), 'HS256'));

            Auth::setUser($decoded->email);
        }
        catch (SignatureInvalidException $exception) {

            $this->setContent([
                'error'     =>  true,
                'message'   =>  $exception->getMessage()
            ]);

            $this->respond();
        }
    }
}


