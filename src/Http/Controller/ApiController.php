<?php

namespace Http\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController {

    protected Response $response;

    protected Request $request;

    protected array $content;

    protected $middleware;

    public function __construct()
    {
        $this->request  = Request::createFromGlobals();;
        $this->response = new Response();

        $this->setContentType();
    }

    public function setMiddleware($middleware)
    {
        (new $middleware)->handle($this->request);
    }

    public function setContent(array $content)
    {
        foreach ($content as $key => $value) {
            $this->content[$key] = $value;
        }
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setStatusCode($code = Response::HTTP_OK)
    {
        $this->response->setStatusCode($code);
    }

    public function setContentType($contentType = 'application/json')
    {
        $this
            ->response
            ->headers
            ->set('Content-Type', $contentType);
    }

    public function respond()
    {
        $this->response->setContent(json_encode($this->getContent()));
        $this->response->send();
        exit(0);
    }

}