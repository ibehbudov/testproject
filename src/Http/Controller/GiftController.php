<?php

namespace Http\Controller;


use Http\Middleware\AuthMiddleware;

class GiftController extends ApiController {

    public function __construct()
    {
        parent::__construct();

        $this->setMiddleware(AuthMiddleware::class);
    }

    public function offer()
    {
        $this->respond();
    }

}