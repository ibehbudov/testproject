<?php

namespace Http\Controller;

use Carbon\Carbon;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Repository\UserRepository;
use Tools\Config;

class LoginController extends ApiController
{
    protected UserRepository $userRepository;

    public function __construct()
    {
        parent::__construct();

        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        \Tools\Jwt::encode(['email' => 'salam']);

        if($this->userRepository->checkCredentials(
            $this->request->request->get('email'),
            $this->request->request->get('password')
        )) {
            $this->setContent(['email'      => $this->userRepository->getUser()->getEmail()]);
            $this->setContent(['token'      => $this->userRepository->generateUserJwtToken()]);
            $this->setContent(['expires_at' => Config::get('jwt.exp')]);
            $this->respond();
        }
        else {
            $this->setContent([
                'error'     =>  true,
                'message'   =>  "Check credentials"
            ]);
        }

        $this->respond();
    }
}