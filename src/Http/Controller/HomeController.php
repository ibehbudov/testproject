<?php

namespace Http\Controller;

use Http\Middleware\AuthMiddleware;

class HomeController extends ApiController {

    public function __construct()
    {
        parent::__construct();

        $this->setMiddleware(AuthMiddleware::class);
    }

    public function index()
    {
        $this->setContent(['salam']);

        $this->respond();
    }

    public function error404()
    {
        $this->setContent([
            'error'     =>  true,
            'message'   =>  "Route not found"
        ]);

        $this->setStatusCode(404);

        $this->respond();
    }

}