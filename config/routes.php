<?php

use Bramus\Router\Router;

$router = new Router();

$router->mount('/api', function () use ($router) {

    $router->setNamespace('\Http\Controller');

    $router->post('login', 'LoginController@login');

    $router->get('/', 'HomeController@index');

    $router->post('gift/offer', 'GiftController@offer');

});

$router->set404('HomeController@error404');

$router->run();