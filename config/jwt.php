<?php

use Carbon\Carbon;
use Tools\Config;

return [
    'iss'   =>  Config::get('app.url'),
    'iat'   =>  Carbon::now()->unix(),
    'exp'   =>  Carbon::now()->addDay()->unix(),
    'key'   =>  "@{AD5aYhJ-j?4`PX)AV(;M#BWSB~j:-*pF?7ZJ9p\^{&NJ}`47P%+'J8v257vf&X.d(2jLZQwR{*tr&kV=zj>Hwz77L'g,EU`[C~]c7}"
];

